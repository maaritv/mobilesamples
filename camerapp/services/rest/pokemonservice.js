export function getPokemons(offset, pageNumber, setData) {
    fetch(
        "https://pokeapi.co/api/v2/pokemon?limit=" + offset + "&offset=" + offset * pageNumber
    )
        .then((response) => response.json())
        .then((data) => {
            //console.log(JSON.stringify(data.results));
            setData(data.results, pageNumber)
        });
}


export const getPokemon = (url) => {
    return fetch(
      url)
      .then((res) => res.json());
  };
