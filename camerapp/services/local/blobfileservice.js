import RNFetchBlob from 'react-native-fetch-blob';

export function getFilePathForBlob(filename){
  console.log("get file name  for "+filename);
  let dirs = RNFetchBlob.fs.dirs;
     return dirs.CacheDir  + "/"+filename;
}

export function getAndSaveBinaryData(url, filename) {
  console.log("storing "+filename);
    return RNFetchBlob
    .config({
      // response data will be saved to this path if it has access right.
      path: filename
    })
    .fetch('GET', url, {
      //some headers ..
    })
    .then((res) => {
      // the path should be dirs.DocumentDir + 'path-to-file.anything'
      console.log('The file saved to ', res.path())
    })
}