var RNFS = require('react-native-fs');

// create a path you want to write to
// :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
// but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable


/**
 * Writes base64 content to file without base64-prefix. It appears to app data folder 
 * like /data/user/0/com.camera_sample/files/
 * 
 * @param content 
 */


export async function writeTxtFile(content, filePath) {
    var path = RNFS.DocumentDirectoryPath + '/' + filePath;

    // write the file
    RNFS.writeFile(path, content)
        .then((success) => {
            console.log("data is stored to " + path);
        })
        .catch((err) => {
            console.log(err.message);
        });
}



export async function readTxtFile(fileName) {
    var filePath = RNFS.DocumentDirectoryPath + '/' + fileName;
    let data = await RNFS.readFile(filePath);
    return data;
}
