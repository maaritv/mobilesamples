import Realm from "realm";

// Declare Schema
export class PokemonSchema extends Realm.Object { }

PokemonSchema.schema = {
    name: 'Pokemons',
    properties: {
        name: 'string',
        url: 'string'
    }
}


export class PokemonsSchema extends Realm.Object { }

PokemonsSchema.schema = {
    name: 'Pokemon',
    primaryKey: 'id',
    properties: {
        id: 'int',
        name: 'string',
        base_experience: 'int',
        weight: 'int',
        imageFile: 'string',
        imageUrl: 'string',
        imageBlobUrl: 'string'
    }
};


// Create realm
let realm = new Realm({ path: 'PokemonDatabase.realm', schema: [PokemonSchema, PokemonsSchema], schemaVersion: 1 });

export function getRealm() {
    return new Realm({ path: 'PokemonDatabase.realm', schema: [PokemonSchema, PokemonsSchema], schemaVersion: 1 });
}

export function writeAllPokemons(pokemons) {
    let realm = getRealm();
    realm.write(() => {
        pokemons.forEach(pokemon => {
            const p = realm.create('Pokemons', pokemon);
        });
    });
}

export function addPokemon(pokemon) {
    let realm = getRealm();
    realm.write(() => {
        const p = realm.create('Pokemon', pokemon);
        console.log("wrote one pokemon");
    });
}



export function updatePokemon(pokemon) {
    let realm = getRealm();
    let existing_pokemon = getPokemonById(pokemon.id)[0];
    realm.write(() => {     
        existing_pokemon.name = pokemon.name;
        existing_pokemon.base_experience = pokemon.base_experience;
        existing_pokemon.weight = pokemon.weight;
    });
}

export function updatePokemonImageFile(pokemon, imageFile) {
    let realm = getRealm();
    realm.write(() => {
        let existing_pokemon = getPokemonById(pokemon.id)[0];
        existing_pokemon.imageFile = imageFile;
    });
}


export function getPokemonById(id) {
    let realm = getRealm();
    return realm.objects('Pokemon').filtered(`id = ${id}`)[0];
}

export function deletePokemonById(id) {
    let realm = getRealm();
    const pokemon = realm.objects('Pokemon').filtered(`id = ${id}`)[0];
    realm.write(() => {
        realm.delete(pokemon);
        console.log("deleted a pokemon " + id);
    });

}

export function deleteAllPokemons() {
    let realm = getRealm();
    realm.write(() => {
        realm.delete(getAllPokemons());
    })
}


export function getAllPokemons() {
    let realm = getRealm();
    const pokemon = realm.objects('Pokemons');
    // console.log("pokemon are " + JSON.stringify(pokemon));
    return pokemon;
}


export function getAllPokemonsWithData() {
    let realm = getRealm();
    const pokemon = realm.objects('Pokemon');
    // console.log("pokemon are " + JSON.stringify(pokemon));
    return pokemon;
}

export function getAllPokemonsWithDataAndNoImageFile() {
    let realm = getRealm();
    const pokemon = realm.objects('Pokemon').filtered(`imageFile = ''`)[0];
    // console.log("pokemon are " + JSON.stringify(pokemon));
    return pokemon;
}

// Export the realm
export default realm;