var RNFS = require('react-native-fs');


export function getFilePathForExtFile(id) {
    return RNFS.ExternalDirectoryPath + '/pokeappi/' + id;
}


/**
 * Writes base64 content to file without base64-prefix. It appears to internal storage 
 * to pokeappi-folder and you can browse it with file manager app. (/storage/emulated/0/Android/data/com.camera_sample/files/pokeappi/)
 * 
 * @param content 
 */

export async function writeImageToExtFile(content, id) {
    var path = RNFS.ExternalDirectoryPath + '/pokeappi/' + id;
    try {
        RNFS.mkdir(RNFS.ExternalDirectoryPath + '/pokeappi');
        console.log("path is created");
    }
    catch (exception) {
        console.log("path was not created " + exception.message);
    }


    // write the file
    RNFS.writeFile(path, 'data:image/jpeg;base64,' + content, 'base64')
        .then((success) => {
          //  console.log("data " + content + " is stored to " + path);
        })
        .catch((err) => {
            console.log(err.message);
        });
}

export async function readImageFromExtFile(pokemon) {
    let data = await RNFS.readFile(pokemon.imageFile, 'base64');
    return data;
}


/**
 * Data will be shown in image gallery. 
 * 
 * @param {} content 
 * @param {*} fileName 
 */

export async function writeImageToGallery(content, fileName) {
    var path = RNFS.PicturesDirectoryPath + '/' + fileName;

    // write the file using RNFetchBlob, which can handle binary data. 
    //If you do this with camera, change camera options from base64 -format 
    //to binary format. But you do not need this (image gallery), if you only want 
    //to show images with your app using Image -component.

}





/**
 * Data is stored to  /storage/emulated/0/pokeappi 
 * @param {D} content 
 * @param {*} fileName 
 */

export async function writeImageToExtStorageFile(content, fileName) {
    var path = RNFS.ExternalStorageDirectoryPath + '/pokeappi/' + fileName;
    try {
        RNFS.mkdir(RNFS.ExternalStorageDirectoryPath + '/pokeappi');
        console.log("path is created");
    }
    catch (exception) {
        console.log("path was not created " + exception.message);
    }

    RNFS.unlink(path).then(res => {
        console.log("deleted previous");
    })
        .catch(err => {
            console.log(err.message, err.code);
        });

    // write the file
    RNFS.writeFile(path, content)
        .then((success) => {
            console.log("data is stored to " + path);
        })
        .catch((err) => {
            console.log(err.message);
        });

}




export function readImageFromExtStorageFile(fileName, setImageFromFS) {
    var filePath = RNFS.ExternalStorageDirectoryPath + '/' + fileName;
    RNFS.readFile(filePath).then(res => {
        setImageFromFS(res)
    })
        .catch(err => {
            console.log(err.message, err.code);
        });
}


