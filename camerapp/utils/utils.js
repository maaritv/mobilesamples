export function findIdFromUrl(url){
    let id = url.substring(0, url.length - 1);
    console.log(id);
    id = id.substring(id.lastIndexOf("/") + 1, id.length);
    console.log(id);
    return id;
}

export function findExtensionFromImageUrl(url){
    if (url.endsWith("png")){
        return "png";
    }
    else if (url.endsWith("jpeg")) {
        return "jpeg";
    }
    else if (url.endsWith("jpg")){
        return "jpg";
    }
}