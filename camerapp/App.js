import 'react-native-gesture-handler';
import React from 'react';
import { Text, SafeAreaView, StyleSheet, StatusBar, FlatList, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ImageScreen } from './screens/ImageScreen';
import { PokemonsScreen } from './screens/PokemonsScreen';
import { PokemonScreen } from './screens/PokemonScreen';
import { CameraScreen } from './screens/CameraScreen';


class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
  }
  render() {
    return (<View style={styles.item}>

      <Text style={{ fontSize: 20 }}
        onPress={() =>
          this.props.navigation.push('Camera', {
            navigation: this.props.navigation,
          })
        }>
        Camera
  </Text>
      <Text style={{ fontSize: 20 }}
        onPress={() =>
          this.props.navigation.push('Pokemons', {
            navigation: this.props.navigation,
          })
        }>
        Pokemons
  </Text>

    </View>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});


export const App = () => {

  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: 'Home' }}
        />
        <Stack.Screen
          name="Image"
          component={ImageScreen}
          options={{ title: 'Image' }}
        />
        <Stack.Screen
          name="Camera"
          component={CameraScreen}
          options={{ title: 'Camera' }}
        />
        <Stack.Screen
          name="Pokemons"
          component={PokemonsScreen}
          options={{ title: 'Pokemons' }}
        />
        <Stack.Screen
          name="Pokemon"
          component={PokemonScreen}
          options={{ title: 'Pokemon' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App
