package com.camerapp;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.jstasks.HeadlessJsTaskConfig;
import javax.annotation.Nullable;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import com.facebook.react.HeadlessJsTaskService;
import android.os.Handler;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.app.ActivityManager;
import java.util.List;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        Log.w("pokemonservice", "************************* NETWORK CHANGE OCCURED *****************************************");
        Log.w("pokemonservice", "************************* NETWORK CHANGE OCCURED***************************************");
        /**
          This part will be called every time network connection is changed
          e.g. Connected -> Not Connected
        **/
        if (!isAppOnForeground((context))) {
            /**
              We will start our service and send extra info about
              network connections
            **/
            boolean hasInternet = isNetworkAvailable(context);
            Log.w("pokemonservice", "*************************APP IN ON BACKGROUND *********************************************************************");
            Log.w("pokemonservice", "*************************APP IN ON BACKGROUND************************************************");

            Intent serviceIntent = new Intent(context, PokemonTaskService.class);
            serviceIntent.putExtra("hasInternet", true);
            context.startService(serviceIntent);
            HeadlessJsTaskService.acquireWakeLockNow(context);
        }
        else {
            Log.w("pokemonservice", "----------------------- APP IS ON FOREGROUND-----------------------------------------------------------------------");
            Log.w("pokemonservice", "----------------------- APP IS ON FOREGROUND----------------------------------------------------");
        }
    }

    private boolean isAppOnForeground(Context context) {
        /**
          We need to check if app is in foreground otherwise the app will crash.
         http://stackoverflow.com/questions/8489993/check-android-application-is-in-foreground-or-not
        **/
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses =
        activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
     
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance ==
            ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
             appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
        
       //return true;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
        context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }


}