import React from 'react';
import {
    ActivityIndicator,
    TouchableOpacity,
    Text,
    View,
    ScrollView,
    Image,
    SafeAreaView,
} from 'react-native';
import { getAllPokemons, writeAllPokemons, deleteAllPokemons } from '../../services/local/pokemonstorageservice';
import { getPokemons } from '../../services/rest/pokemonservice';
import { PokemonsView, createTableData, MiniPokemon, IconTable } from './pokemongridcomponents'; 


export class PokemonsGrid extends React.Component {

    constructor(props) {
        super(props);
        console.log(JSON.stringify(this.props));
        this.state = {
            pokemons: [],
            flagData: '',
            loading: true,
            error: false,
            plainmode: false,
            currentPage: 0
        };
    }


    showPokemon = (url, image) => {
        this.props.navigation.push('Pokemon', {
            url: url,
            imageUrl: image,
            navigation: this.props.navigation.route,
        })
    }



    setPokemons = (pokemons, pageNumber) => {
        this.setState({
            pokemons: pokemons,
            loading: false,
            currentpage: pageNumber
        });
        deleteAllPokemons();
        writeAllPokemons(pokemons);
    }


    getData(pageNumber) {
        getPokemons(50, pageNumber, this.setPokemons)
    }

    componentDidMount() {
        const pokemons = getAllPokemons();
        //Show pokemons from storage, if they are there.
        if (pokemons !== undefined) {
            this.setState({
                pokemons: pokemons,
                loading: false,
            });
        }
        //load from REST.
        this.getData(0);
    }

    render() {
        if (this.state.loading) {
            return <ActivityIndicator size="large" color="#00ff00"/>;
        }
        if (this.state.pokemons.length == 0) {
            return (
                <View>
                    <Text>No pokemons found from net or storage. Check that you are online.</Text>
                </View>
            );
        } else {
            return (
                <PokemonsView pokemons={this.state.pokemons} showPokemon={this.showPokemon} />
            );
        }
    }
}
