import React from 'react';
import {
    ActivityIndicator,
    TouchableOpacity,
    Text,
    View,
    ScrollView,
    Image,
    SafeAreaView,
} from 'react-native';
import { Table, Rows } from 'react-native-table-component';
import { findIdFromUrl } from '../../utils/utils';


export const PokemonsView = ({ pokemons, showPokemon }) => {
    return (
        <ScrollView>
            <IconTable pokemons={pokemons} showPokemon={showPokemon} />
        </ScrollView>
    );
};

export function createTableData(pokemons, cols, showPokemon) {
    let data = new Array();
    let i = 0;
    let j = 0;
    let row = new Array();
    //console.log("pokemonit " + JSON.stringify(pokemons));
    pokemons.map(pokemon => {
        if (i < cols) {
            row.push(MiniPokemon({ pokemon, showPokemon }));
        }
        if (i == cols) {
            data[j] = row;
            console.log(data[j])
            row = new Array()
            i = -1;
            j++;
        }
        i++;
    })
    return data;
}

export const MiniPokemon = ({ pokemon, showPokemon }) => {
    let id = findIdFromUrl(pokemon.url);
    //console.log("id is " + id);
    const image = `https://pokeres.bastionbot.org/images/pokemon/${id}.png`;
    return (<View style={{ height: 130 }}>
        <TouchableOpacity onPress={() => showPokemon(pokemon.url, image)}>
            <Text>{pokemon.name.toUpperCase()}</Text>
            <Image style={{ height: 90, width: 90, padding: 20, alignSelf: 'center' }}
                source={{ uri: image }}
            />
        </TouchableOpacity>
    </View>)
}


export const IconTable = ({ pokemons, showPokemon }) => {

    const tableData = createTableData(pokemons, 3, showPokemon);
    return (
        <SafeAreaView>
            <View><Text style={{ fontSize: 40, fontFamily: 'Roboto', paddingBottom: 10 }}>Pokemons</Text></View>
            <Table borderStyle={{ borderWidth: 1, borderColor: '#000000' }}>
                <Rows data={tableData} />
            </Table>
        </SafeAreaView>
    );
};

