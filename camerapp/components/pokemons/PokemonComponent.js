import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Table, Rows } from 'react-native-table-component';
import { findIdFromUrl, findExtensionFromImageUrl } from '../../utils/utils';
import { addPokemon, getPokemonById } from '../../services/local/pokemonstorageservice';
import { readImageFromExtFile } from '../../services/local/fileserviceexternaldata';
import { getAndSaveBinaryData, getFilePathForBlob } from '../../services/local/blobfileservice';
import { PokemonView } from './pokemoncomponents';
import { getPokemon } from '../../services/rest/pokemonservice';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff',
    margin: 'auto',
  },
  tinyLogo: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: 300,
    height: 180,
    marginBottom: 20,
  },
});



export class PokemonComponent extends React.Component {
  constructor(props) {
    super(props);
    console.log("-----------------CONSTRUCTOR-" + JSON.stringify(this.props) + "--------------------");
    this.state = {
      pokemon: {},
      flagData: '',
      loading: true,
      error: false,
      plainmode: false,
      imageUrl: '',
      imageFile: '',
      errormessage: '',
      imageData: ''
    };
  }

  handleResponse = ({ data, imageUrl }) => {
    //console.log("handleResponse " + imageUrl);
    //console.log(JSON.stringify(data));
    let pokemon = data;
    pokemon.imageFile = '';
    pokemon.imageUrl = imageUrl;
    this.setState({ imageData: pokemon.imageUrl, pokemon: data, loading: false });
    addPokemon(pokemon);
  }

  componentWillUnmount() {
    console.log("-------------component will unmount-" + this.state.pokemon.name + "------------------------------");
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("------------- should component update -" + this.state.pokemon.name + "/" + JSON.stringify(nextState.pokemon.name) + "------------------------------");
    if (nextState.pokemon.name !== this.state.pokemon.name) {
      return true;
    }
    return false;
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    console.log("-------------error happended-" + this.state.pokemon.name + "------------------------------");
    return { errormessage: error };
  }

  async setImageData({ pokemon }) {
    if (pokemon.imageFile !== '') {
      console.log("image file is " + pokemon.imageFile);
      readImageFromExtFile(pokemon).then((data) => {
        // console.log("pokedata is " + data);
        this.setState({ pokemon: pokemon, imageData: data, loading: false });
      })
    }
    else {
      this.setState({ imageData: pokemon.imageUrl, pokemon: pokemon, loading: false })
    }
  }

  async componentDidMount() {
    console.log("-----------------component did mount-" + this.props.route.params.url + "--------------------");
    const url = this.props.route.params.url;
    const imageUrl = this.props.route.params.imageUrl;

    const id = findIdFromUrl(url);
    const imageExtension = findExtensionFromImageUrl(this.props.route.params.imageUrl);

    try {
      let pokemon = getPokemonById(id);

      //If pokemon is not found from storage, retrieve it from REST.
      if (pokemon === undefined) {
        const blobFileName = getFilePathForBlob(id + "." + imageExtension);
        console.log("blob file path is " + blobFileName);
        getAndSaveBinaryData(imageUrl, blobFileName);
        getPokemon(url)
        .then((data) => {
          data.imageBlobUrl = blobFileName;
          this.handleResponse({ data, imageUrl })
        }
        )
      }
      else {
        console.log("pokemon was found " + JSON.stringify(pokemon));
        //Show pokemon from local storage, if it is there.
        await this.setImageData({ pokemon });
      }
    }
    catch (error) {
      this.setState({ error: true, errormessage: "Could not load pokemon from net or from disk " + error.message, loading: false })
    }
  }



  render() {
    console.log("----------------rendering pokemon " + this.state.pokemon.name + "!!!----------------------");
    if (this.state.loading) {
      return <ActivityIndicator size="large" color="#00ff00" />;
    }
    if (this.state.error) {
      return (
        <View>
          <Text>Error {this.state.errormessage} happened.</Text>
        </View>
      );
    } else {
      return (
        <PokemonView pokemon={this.state.pokemon} imageData={this.state.imageData} />
      );
    }
  }
}
