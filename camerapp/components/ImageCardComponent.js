import React from 'react';
import { Text, SafeAreaView, StyleSheet, StatusBar, FlatList, View } from 'react-native';
import { ImageCard } from './ImageCard';
import { writeImageToExtStorageFile, writeImageToGallery } from '../services/local/fileserviceexternaldata';
import { writeTxtFile, readTxtFile } from '../services/local/fileserviceappdata';

const renderItem = ({ item }) => (
    <Item name={item.name} />
);

const Item = ({ name }) => (
    <View style={styles.item}>
        <Text style={styles.title}>{name}</Text>
    </View>
);


export class ImageCardComponent extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            image: {},
            name: "",
            description: ""
        }
    }


    componentDidMount() {
        //const image = require('../assets/images/background.jpg');
        const image = this.props.image;
        console.log("image is "+JSON.stringify(image));
        //this.setState({ image: image.base64 });
        this.setState({ name: "Esimerkki 2" })
        this.setState({ description: "Kuvaus" })
         writeTxtFile(image.base64, this.props.path);
        //If you use database for storing data where image is related, 
        //this is a right place to add call to Realm-function, and 
        //update image path of the entity.
        console.log("READ TEXT FILE "+this.props.path);
        readTxtFile(this.props.path)
        .then((img) => this.setState({image: img}))
    }

    /**
     * Note that image is in base64-format. Then it first needs to be converted to 
     * binary format, before you can store it to image gallery. This is not needed, 
     * if you only want to show the image in Image-component of the app. Then base64 format 
     * is ok to that purpose.
     */
    saveImage = () => {
        //console.log("saving image!! " + this.state.image);
        //const pathToBlobFile = convertBase64ImageToBinaryFormat(this.state.image);
        //writeImageToGallery(pathToBlobFile);
    }


    render() {
        return (
            <ImageCard saveImage={this.saveImage} name={this.state.name} navigation={this.props.navigation} description={this.state.description} image={this.state.image} />
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
});