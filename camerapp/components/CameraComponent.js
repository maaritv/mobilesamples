import React, { useState, useEffect } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
} from 'react-native';

import { RNCamera } from 'react-native-camera'

export class CameraComponent extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            path: ''
        }
    }

    savePath = (pth) => {
        console.log("got path "+pth);
        this.setState({ path: pth })
    }


    takePicture = async () => {
        if (this.camera) {
            const options = {
                quality: 0.5, base64: true,
                exif: true
                //    path: ""
            };
            //Replace tempimage.txt with the Realm-entity id of the 
            //entity for what image is taken. Using base64-format 
            //it will be in base64 format, which is text based format.
            //Using id, you can link the image to the right entity.
            let fn= "tempimageXXXXXX.txt";
            const data = await this.camera.takePictureAsync(options);
            this.props.navigation.push('Image', {
                path: fn,
                image: data,
                navigation: this.props.navigation,
            })
        }
    };

    render() {
        return (<SafeAreaView style={styles.container}>
            <RNCamera
                style={{ flex: 1, alignItems: 'center' }}
                ref={ref => {
                    this.camera = ref
                }}
            />
            <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                    <Text style={{ fontSize: 14 }}> SNAP </Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>)
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});

