import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import { getCountries, getCountry } from '../services/countryservice';




export class DataTable extends Component {


  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Property', 'Value'],
      tableData: [],
      loading: true
    }
  }


  createTableData = (country) => {
    let tableData = [];
    tableData[0] = ['Name', country.name];
    tableData[1] = ['Capital', country.capital];
    tableData[2] = ['Sub region', country.subregion];
    tableData[3] = ['Native name', country.nativeName];
    tableData[4] = ['Population', country.population];
    tableData[5] = ['Borders', JSON.stringify(country.borders)];
    console.log(JSON.stringify(tableData));
    this.setState({ tableData: tableData, loading: false });
  }



  async componentDidMount() {
    const country = await getCountry('fi');
    this.createTableData(country);

    /*   getCountry('fi').then(country => {
         this.createTableData(country);
       }) */
  }


  render() {
    const state = this.state;

    if (this.state.loading) {
      return <View><Text>Loading...please wait</Text></View>
    }
    else {
      return (
        <View style={styles.container}>
          <Table borderStyle={{ borderWidth: 2, borderColor: '#c8e1ff' }}>
            <Row data={state.tableHead} style={styles.head} textStyle={styles.text} />
            <Rows data={state.tableData} textStyle={styles.text} />
          </Table>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: { margin: 6 }
});