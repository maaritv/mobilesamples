import React, { Component } from 'react';
import { View, Text } from 'react-native';


export const Header = ({ text }) => {

    const styles = {

        header: {
            fontSize: 20,
            color: 'green',
            alignSelf: 'center',
            fontWeight: 'bold'
        }
    }

    return (<View><Text style={styles.header}>{text}</Text></View>)
}