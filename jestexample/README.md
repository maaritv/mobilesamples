# jestexample

Demonstrates use of JEST and Enzyme with React Native app.Project contains few components and functions.
Following is demonstrated:

* Use of mocking native resources and functions
* testing of state changes
* testing of component rendering

Installation
==============================================
Change to jestexample directory and run:

   npm install 

Running tests
=============================================
  npm test

Running the project
=============================================

react-native run-android or 
react-native run-ios