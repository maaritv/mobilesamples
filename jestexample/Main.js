import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import { DataTable } from './components/DataTable';
import { Header } from './components/datacomponents.js';

export class Main extends React.Component {


    constructor(props) {
        super(props);
        //console.log(JSON.stringify(props));
        this.state = {
            tableData: [],
            code: 'fi',
            error: '',
            isLoading: true
        };
    }

    
    render() {
        return (<ScrollView><Header text="Tiedot"/><DataTable/></ScrollView>);
    }
}
export default Main;
