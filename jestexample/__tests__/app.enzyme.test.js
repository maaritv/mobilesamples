/**
 * @jest-environment jsdom
 */
import React from 'react';
import { mount, shallow, render } from 'enzyme';
import { App } from '../App';
import { Main } from '../Main';
import { Header } from '../components/datacomponents';
import { DataTable } from '../components/DataTable';
import { getCountry } from '../services/countryservice';


describe('Test app tree', () => {

    beforeEach(() => {
        jest.clearAllMocks();
        fetch.doMock();
    }
    )

    test('Create app. it should not crash', () => {
        const component = shallow(<App />);
        expect(component).toMatchSnapshot();
    })

    test('Find DataTable element', () => {
        const wrapper = shallow(<Main />);
        const header = <Header text="Tiedot" />;
        const dataTable = <DataTable />;
        expect(wrapper.contains(dataTable)).toEqual(true);
        expect(wrapper.contains(header)).toEqual(true);
    })

    it("Check text props from Header", () => {
        const wrapper = mount(<Header text="Tiedot" />);
        expect(wrapper.props().text).toEqual("Tiedot");
    });

    it("Check if componentDidMount is called", () => {
        const spy = jest.spyOn(DataTable.prototype, 'componentDidMount');  // spy on customFunc using the prototype
        const wrapper = mount(<DataTable />);
        expect(spy).toHaveBeenCalledTimes(1);
    });


    it('Check getCountries fetch is called', () => {
        require('jest-fetch-mock').enableMocks()
        const mockResponse = {
            name: 'Finland',
            capital: 'Helsinki',
            subregion: 'Northern Europe',
            nativeName: 'Suomi',
            population: 5491817,
            borders: ["NOR", "SWE", "RUS"]
        }

        fetch.mockResponseOnce(JSON.stringify(mockResponse));

        getCountry('fi').then(res => {
            expect(res).toEqual(mockResponse)
        })

        expect(fetch.mock.calls.length).toEqual(1)
        expect(fetch.mock.calls[0][0]).toEqual('https://restcountries.eu/rest/v2/alpha/fi')
    });


    it("Check DataTable tableHead state when component is mounted", () => {
        const wrapper = mount(<DataTable />);
        expect(wrapper.state("tableHead")).toEqual(['Property', 'Value']);
    });

})