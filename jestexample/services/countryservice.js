



export const getCountry = (code) => {
    return fetch(
      `https://restcountries.eu/rest/v2/alpha/${code}`)
      .then((res) => res.json());
  };